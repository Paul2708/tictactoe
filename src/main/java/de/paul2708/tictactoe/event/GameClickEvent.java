package de.paul2708.tictactoe.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import de.paul2708.tictactoe.game.Game;

public class GameClickEvent extends Event {

	private static final HandlerList handlers = new HandlerList();

	private Player p;
	private Game game;
	private ItemStack item;
	private Inventory inv;
	private int slot;
	
	public GameClickEvent(Player p, Game game, ItemStack item, Inventory inv, int slot) {
		this.p = p;
		this.game = game;
		this.item = item;
		this.inv = inv;
		this.slot = slot;
	}

	public Player getPlayer() {
		return p;
	}

	public Game getGame() {
		return game;
	}

	public Inventory getInventory() {
		return inv;
	}
	
	public ItemStack getClickedItem() {
        return item;
	}
	
	public Inventory getClickedInventory() {
        return inv;
	}
	
	public int getSlot() {
        return slot;
	}
	
	public HandlerList getHandlers() {
        return handlers;
	}

	public static HandlerList getHandlerList() {
        return handlers;
	}
}
