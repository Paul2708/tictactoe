package de.paul2708.tictactoe.command;

import de.paul2708.tictactoe.TicTacToe;
import de.paul2708.tictactoe.game.Queue;
import de.paul2708.tictactoe.util.Constants;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 05.08.2017.
 */
public class TicTacToeCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;
            Queue queue = TicTacToe.getGameManager().getQueue();

            if (queue.contains(player)) {
                queue.removePlayer(player);
                player.sendMessage(TicTacToe.getMessageFile().getMessage("queue.leave"));
            } else {
                queue.addPlayer(player);

                if (queue.getSize() == 2) {
                    Player first = queue.getPlayer(0);

                    TicTacToe.getGameManager().createGame(first, player);

                    queue.removeAll();
                    return true;
                }

                player.sendMessage(TicTacToe.getMessageFile().getMessage("queue.added"));
            }
        } else {
            TicTacToe.getInstance().log(Constants.TAG + "§cYou cannot run this command by console.");
        }

        return true;
    }
}
