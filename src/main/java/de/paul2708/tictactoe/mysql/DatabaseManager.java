package de.paul2708.tictactoe.mysql;

import de.paul2708.tictactoe.TicTacToe;
import de.paul2708.tictactoe.stats.Profile;
import de.paul2708.tictactoe.util.Constants;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import static de.paul2708.tictactoe.TicTacToe.getMySQL;

/**
 * Created by Paul on 07.08.2016.
 */
public class DatabaseManager {

    // Tables
    public static void setupTables() {
        String query = "CREATE TABLE IF NOT EXISTS `ttt_stats` ("
                + "`uuid` varchar(36) NOT NULL,"
                + "`games` int(11) NOT NULL DEFAULT '0',"
                + "`wins` int(11) NOT NULL DEFAULT '0',"
                + "`loses` int(11) NOT NULL DEFAULT '0',"
                + "`draws` int(11) NOT NULL DEFAULT '0',"
                + "UNIQUE KEY `uuid` (`uuid`)) "
                + "ENGINE=InnoDB DEFAULT CHARSET=latin1";
        getMySQL().queryUpdate(query);
    }

    // Get data
    public static void resolveStats() {
        if (!TicTacToe.getMySQL().hasConnection()) {
            TicTacToe.getMySQL().openConnection();
        }
        ResultSet rs = TicTacToe.getMySQL().getQuery("SELECT * FROM `ttt_stats`");
        try {
            while (rs.next()) {
                UUID uuid = UUID.fromString(rs.getString("uuid"));

                Profile profile = new Profile(uuid);
                profile.load(rs.getInt("games"), rs.getInt("wins"), rs.getInt("loses"), rs.getInt("draws"));
            }
        } catch (SQLException e) {
            TicTacToe.getInstance().log(Constants.TAG + "§cStats cannot be cached.");
            e.printStackTrace();
        }
    }

    // Update values
    public static void update(UUID uuid, String key) {
        if (!TicTacToe.getMySQL().hasConnection()) {
            TicTacToe.getMySQL().openConnection();
        }
        TicTacToe.getInstance().getServer().getScheduler().runTaskAsynchronously(TicTacToe.getInstance(), () -> {
            try {
                PreparedStatement update = TicTacToe.getMySQL().getConnection().prepareStatement(
                        "UPDATE `ttt_stats` SET `" + key + "` = `" + key + "` + 1 WHERE `uuid` = ?");
                update.setString(1, uuid.toString());

                update.executeUpdate();
            } catch (Exception e) {
                TicTacToe.getInstance().log(Constants.TAG + "§cStats for " + uuid + " cannot be saved.");
                e.printStackTrace();
            }
        });
    }

    // Register a player
    public static void register(UUID uuid) {
        if (!TicTacToe.getMySQL().hasConnection()) {
            TicTacToe.getMySQL().openConnection();
        }
        TicTacToe.getInstance().getServer().getScheduler().runTaskAsynchronously(TicTacToe.getInstance(), () -> {
            try {
                PreparedStatement update = TicTacToe.getMySQL().getConnection().prepareStatement(
                        "INSERT IGNORE INTO `ttt_stats` (`uuid`, `games`, `wins`, `loses`, `draws`) "
                                + "VALUES (?, '0', '0', '0', '0')");
                update.setString(1, uuid.toString());

                update.executeUpdate();
            } catch (Exception e) {
                TicTacToe.getInstance().log(Constants.TAG + "§cStats for " + uuid + " cannot be registered.");
                e.printStackTrace();
            }
        });
    }
}
