package de.paul2708.tictactoe.mysql;

import de.paul2708.tictactoe.TicTacToe;
import de.paul2708.tictactoe.util.Constants;

import java.sql.*;

/**
 * Created by Paul on 07.08.2016.
 */
public class MySQL {

    private Connection connection;
    private String host = "localhost", database, user, password;
    private int port = 3306;

    public MySQL(String host, String database, String user, String password) {
        this.host = host;
        this.database = database;
        this.user = user;
        this.password = password;
    }

    public Connection openConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.connection = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database + "?user=" + this.user + "&password=" + this.password + "&autoReconnect=true");
            TicTacToe.getInstance().log(Constants.TAG + "§aSuccessfully connected to database '" + this.database + "'.");
        } catch (SQLException e) {
            TicTacToe.getInstance().log(Constants.TAG + "§cCannot connect to database '" + this.database + "'.");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            TicTacToe.getInstance().log(Constants.TAG + "§cMySQL-Driver cannot be found.");
        }

        return this.connection;
    }

    public boolean hasConnection() {
        try {
            return (this.connection != null) || (this.connection.isValid(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public void queryUpdate(String query) {
        if (!hasConnection()) {
            openConnection();
        }
        Connection con = MySQL.this.connection;
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(query);
            st.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            MySQL.this.closeResources(null, st);
        }
    }

    public ResultSet getQuery(String query) {
        if (!hasConnection()) {
            openConnection();
        }
        try {
            PreparedStatement stmt = this.connection.prepareStatement(query);
            return stmt.executeQuery();
        } catch(Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void closeResources(ResultSet rs, PreparedStatement st) {
        try {
            if(rs != null) rs.close();
            if(st != null) st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void closeConnection() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            this.connection = null;
        }
    }

    public Connection getConnection() {
        return connection;
    }
}
