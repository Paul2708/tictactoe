package de.paul2708.tictactoe;

import de.paul2708.tictactoe.command.TicTacToeCommand;
import de.paul2708.tictactoe.file.ConfigFile;
import de.paul2708.tictactoe.file.MessageFile;
import de.paul2708.tictactoe.game.GameManager;
import de.paul2708.tictactoe.listener.*;
import de.paul2708.tictactoe.mysql.DatabaseManager;
import de.paul2708.tictactoe.mysql.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class TicTacToe extends JavaPlugin {

	@Override
	public void onLoad() {
		TicTacToe.instance = this;
	}
	
	@Override
	public void onEnable() {
		// Config
		ConfigFile config = new ConfigFile(getDataFolder());
		config.load();
		TicTacToe.messageFile = new MessageFile(getDataFolder());
		TicTacToe.messageFile.load();

		// Game manager
		TicTacToe.gameManager = new GameManager();

		// MySQL
		TicTacToe.mySQL = new MySQL(config.getIp(), config.getDatabase(), config.getUser(), config.getPassword());
		mySQL.openConnection();

		DatabaseManager.setupTables();
		DatabaseManager.resolveStats();

		// Commands
		getCommand("tictactoe").setExecutor(new TicTacToeCommand());

		// Listener
		registerListener();
	}

	@Override
	public void onDisable() {
		if (mySQL != null && mySQL.hasConnection()) {
			mySQL.closeConnection();
		}
	}

	private static TicTacToe instance;
	private static MessageFile messageFile;
	private static GameManager gameManager;
	private static MySQL mySQL;

	public static TicTacToe getInstance() {
		return instance;
	}

	public static MessageFile getMessageFile() {
		return messageFile;
	}

	public static GameManager getGameManager() {
		return gameManager;
	}

	public static MySQL getMySQL() {
		return mySQL;
	}

	public void log(String message) {
		Bukkit.getConsoleSender().sendMessage(message);
	}

	private void registerListener() {
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new InventoryClickListener(), this);
		pm.registerEvents(new GameClickListener(), this);
		pm.registerEvents(new PlayerInteractListener(), this);
		pm.registerEvents(new PlayerJoinListener(), this);
		pm.registerEvents(new InventoryCloseListener(), this);
		pm.registerEvents(new PlayerQuitListener(), this);
	}

}
