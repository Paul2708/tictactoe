package de.paul2708.tictactoe.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import de.paul2708.tictactoe.TicTacToe;

public class PlayerQuitListener implements Listener {

	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		
		if (TicTacToe.getGameManager().getQueue().contains(p)) {
			TicTacToe.getGameManager().getQueue().removePlayer(p);
		}
	}
}
