package de.paul2708.tictactoe.listener;

import de.paul2708.tictactoe.mysql.DatabaseManager;
import de.paul2708.tictactoe.stats.Profile;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player player = e.getPlayer();

		Profile profile = Profile.getProfile(player.getUniqueId());

		if (profile == null) {
			new Profile(player.getUniqueId());

			DatabaseManager.register(player.getUniqueId());
		}
	}
}
