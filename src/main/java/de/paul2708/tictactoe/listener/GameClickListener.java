package de.paul2708.tictactoe.listener;

import de.paul2708.tictactoe.TicTacToe;
import de.paul2708.tictactoe.event.GameClickEvent;
import de.paul2708.tictactoe.game.Game;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class GameClickListener implements Listener {

	@EventHandler
	public void onClick(GameClickEvent e) {
		Player player = e.getPlayer();
		Game game = e.getGame();

		ItemStack item = e.getClickedItem();
		Inventory inv = e.getClickedInventory();
		int slot = e.getSlot();
		
		// game.inv = inv;
		
		if(e.getClickedInventory().getType() != InventoryType.WORKBENCH || game.hasEnded()) {
            return;
        }

		Player turn = game.getTurn();

		if(!(turn.getUniqueId().equals(player.getUniqueId()))) {
			player.sendMessage(TicTacToe.getMessageFile().getMessage("game.not_your_turn"));
			return;
		}

		if(item == null || item.getType() == null) {
			if (slot == 0) {
				player.sendMessage(TicTacToe.getMessageFile().getMessage("game.click_nowhere"));
				return;
			}

			game.place(slot);
		}
	}
}
