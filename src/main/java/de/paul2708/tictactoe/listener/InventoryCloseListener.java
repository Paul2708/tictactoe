package de.paul2708.tictactoe.listener;

import de.paul2708.tictactoe.TicTacToe;
import de.paul2708.tictactoe.game.Game;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class InventoryCloseListener implements Listener {

	@EventHandler
	public void onClose(InventoryCloseEvent e) {
		if (e.getPlayer() instanceof Player) {
			Player player = (Player) e.getPlayer();

			Game game = TicTacToe.getGameManager().getGame(player);
			if(game == null || game.hasEnded()) {
				return;
			}

			for (Player all : game.getPlayers()) {
				if (!(all.getUniqueId().equals(player.getUniqueId()))) {
					game.win(all);
					break;
				}
			}
		}
	}
}
