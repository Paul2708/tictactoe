package de.paul2708.tictactoe.listener;

import de.paul2708.tictactoe.TicTacToe;
import de.paul2708.tictactoe.game.Queue;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteractListener implements Listener {

	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block block = e.getClickedBlock();

			if (block.getType() == Material.WORKBENCH) {
				e.setCancelled(true);
				Queue queue = TicTacToe.getGameManager().getQueue();

				if (queue.contains(p)) {
					queue.removePlayer(p);
					p.sendMessage(TicTacToe.getMessageFile().getMessage("queue.leave"));
				} else {
					queue.addPlayer(p);

					if (queue.getSize() == 2) {
						Player first = queue.getPlayer(0);
						TicTacToe.getGameManager().createGame(first, p);

						queue.removeAll();
					} else {
						p.sendMessage(TicTacToe.getMessageFile().getMessage("queue.added"));
					}
				}
			}
		}
	}
}
