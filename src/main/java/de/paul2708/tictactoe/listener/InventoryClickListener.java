package de.paul2708.tictactoe.listener;

import de.paul2708.tictactoe.TicTacToe;
import de.paul2708.tictactoe.event.GameClickEvent;
import de.paul2708.tictactoe.game.Game;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryClickListener implements Listener {

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if (e.getWhoClicked() instanceof Player) {
			Player player = (Player) e.getWhoClicked();

			if (e.getClickedInventory() == null) {
				return;
			}
			
			Game game = TicTacToe.getGameManager().getGame(player);
			if (game == null) {
				return;
			}
			
			e.setCancelled(true);

			Event event = new GameClickEvent(player, game, e.getCurrentItem(),
					e.getClickedInventory(), e.getSlot());
			Bukkit.getPluginManager().callEvent(event);
		}
	}
}
