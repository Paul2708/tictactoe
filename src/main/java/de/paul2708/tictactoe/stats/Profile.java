package de.paul2708.tictactoe.stats;

import de.paul2708.tictactoe.mysql.DatabaseManager;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 01.08.2017.
 */
public class Profile {

    private static List<Profile> cache = new CopyOnWriteArrayList<>();

    private UUID uuid;

    private int games;
    private int wins;
    private int loses;
    private int draws;

    public Profile(UUID uuid) {
        this.uuid = uuid;

        this.games = 0;
        this.wins = 0;
        this.loses = 0;
        this.draws = 0;

        Profile.cache.add(this);
    }

    public void load(int games, int wins, int loses, int draws) {
        this.games = games;
        this.wins = wins;
        this.loses = loses;
        this.draws = draws;
    }

    public void addGame() {
        games++;

        DatabaseManager.update(uuid, "games");
    }

    public void addWin() {
        wins++;

        DatabaseManager.update(uuid, "wins");
    }

    public void addLose() {
        loses++;

        DatabaseManager.update(uuid, "loses");
    }

    public void addDraw() {
        draws++;

        DatabaseManager.update(uuid, "draws");
    }

    public UUID getUuid() {
        return uuid;
    }

    public int getGames() {
        return games;
    }

    public int getWins() {
        return wins;
    }

    public int getLoses() {
        return loses;
    }

    public int getDraws() {
        return draws;
    }

    public static Profile getProfile(UUID uuid) {
        for (Profile profile : cache) {
            if (profile.getUuid().equals(uuid)) {
                return profile;
            }
        }

        return null;
    }
}
