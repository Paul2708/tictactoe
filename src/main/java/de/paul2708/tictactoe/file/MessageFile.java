package de.paul2708.tictactoe.file;

import de.paul2708.tictactoe.TicTacToe;
import de.paul2708.tictactoe.stats.Profile;
import de.paul2708.tictactoe.util.Constants;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

/**
 * Created by Paul on 28.07.2017.
 */
public class MessageFile {

    private File directory;
    private File configFile;
    private YamlConfiguration configuration;

    private boolean first;

    public MessageFile(File directory) {
        this.directory = directory;

        this.first = false;
    }

    public void load() {
        try {
            // Create directory
            if (!directory.exists()) {
                directory.mkdir();
            }

            // Create file
            this.configFile = new File(directory.getPath(), "messages.yml");
            if (!configFile.exists()) {
                configFile.createNewFile();
                this.first = true;

                TicTacToe.getInstance().log(Constants.TAG + "§amessages.yml was created. §cEdit and restart your server.");
            }

            // Load configuration
            this.configuration = YamlConfiguration.loadConfiguration(configFile);

            // Create default value
            if (first) {
                createDefaultValues();
            }

            // Check updates
            checkUpdates();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createDefaultValues() {
        configuration.set("tag", "&8[&eTicTacToe&8]");

        configuration.set("game.against", "%tag% &7You are playing against &e%player%&7.");
        configuration.set("game.first_turn", "%tag% &e%player% &7starts.");
        configuration.set("game.not_your_turn", "%tag% &cIt's not your turn.");
        configuration.set("game.click_nowhere", "%tag% &cPlease just use the workbench.");

        configuration.set("queue.leave", "%tag% &cYou left the queue.");
        configuration.set("queue.added", "%tag% &aYou joined the queue. &7Waiting for player..");

        configuration.set("result.draw", "%tag% &eDraw! - Nobody won.");
        configuration.set("result.win", "%tag% &e%player% has won the game.");

        configuration.set("stats.hover", "&7Your stats:\n" +
                "&7Games: &e%games%\n" +
                "&7Wins: &e%wins%\n" +
                "&7Loses: &e%loses%\n" +
                "&7Draws: &e%draws%");
        configuration.set("stats.message", "%tag% &eYour new statistics!");

        try {
            configuration.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void checkUpdates() {
        if (configuration.getString("queue.leave") == null) {
            configuration.set("queue.leave", "%tag% &cYou left the queue.");
            configuration.set("queue.already_in", null);

            try {
                configuration.save(configFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getMessage(String path, String... replace) {
        String message = configuration.getString(path);

        message = message.replaceAll("%tag%", configuration.getString("tag"));

        if (replace.length != 0) {
            message = message.replaceAll("%player%", replace[0]);
        }

        message = ChatColor.translateAlternateColorCodes('&', message);

        return message;
    }

    public String getMessage(String path, Profile profile) {
        String message = getMessage(path);
        message = message.replaceAll("%games%", profile.getGames() + "");
        message = message.replaceAll("%wins%", profile.getWins() + "");
        message = message.replaceAll("%loses%", profile.getLoses() + "");
        message = message.replaceAll("%draws%", profile.getDraws() + "");

        return message;
    }
}
