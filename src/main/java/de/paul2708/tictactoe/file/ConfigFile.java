package de.paul2708.tictactoe.file;

import de.paul2708.tictactoe.TicTacToe;
import de.paul2708.tictactoe.util.Constants;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

/**
 * Created by Paul on 28.07.2017.
 */
public class ConfigFile {

    private File directory;
    private File configFile;
    private YamlConfiguration configuration;

    private boolean first;

    public ConfigFile(File directory) {
        this.directory = directory;

        this.first = false;
    }

    public void load() {
        try {
            // Create directory
            if (!directory.exists()) {
                directory.mkdir();
            }

            // Create file
            this.configFile = new File(directory.getPath(), "config.yml");
            if (!configFile.exists()) {
                configFile.createNewFile();
                this.first = true;

                TicTacToe.getInstance().log(Constants.TAG + "§aconfig.yml was created. §cPlease restart the server.");
            }

            // Load configuration
            this.configuration = YamlConfiguration.loadConfiguration(configFile);

            // Create default value
            if (first) {
                createDefaultValues();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createDefaultValues() {
        configuration.set("mysql.ip", "localhost");
        configuration.set("mysql.database", "tictactoe");
        configuration.set("mysql.user", "root");
        configuration.set("mysql.password", "");

        try {
            configuration.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getIp() {
        return configuration.getString("mysql.ip");
    }

    public String getDatabase() {
        return configuration.getString("mysql.database");
    }

    public String getUser() {
        return configuration.getString("mysql.user");
    }

    public String getPassword() {
        return configuration.getString("mysql.password");
    }
}
