package de.paul2708.tictactoe.game;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Queue {

	private List<UUID> list;
	
	public Queue() {
		list = new ArrayList<>();
	}
	
	public void addPlayer(Player p) {
		list.add(p.getUniqueId());
	}
	
	public void removePlayer(Player p) {
		list.remove(p.getUniqueId());
	}
	
	public boolean contains(Player p) {
		return list.contains(p.getUniqueId());

	}
	
	public int getSize() {
		return list.size();
	}
	
	public Player getPlayer(int index) {
		UUID uuid = list.get(index);
		return Bukkit.getPlayer(uuid);
	}
	
	public void removeAll() {
		list.clear();
	}
}
