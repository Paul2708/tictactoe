package de.paul2708.tictactoe.game;

import de.paul2708.tictactoe.TicTacToe;
import de.paul2708.tictactoe.stats.Profile;
import de.paul2708.tictactoe.util.Util;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class Game {

	private Player[] players;

	public Inventory inventory;
	private int turn;

	private boolean ended;

	public Game(Player first, Player second) {
		this.players = new Player[] { first, second };

		this.inventory = Bukkit.createInventory(null, InventoryType.WORKBENCH);
		this.turn = Util.random.nextInt(2);

		this.ended = false;

		// Send messages
		players[0].sendMessage(TicTacToe.getMessageFile().getMessage("game.against", players[1].getName()));
		players[1].sendMessage(TicTacToe.getMessageFile().getMessage("game.against", players[0].getName()));
		
		sendMessage(TicTacToe.getMessageFile().getMessage("game.first_turn", players[turn].getName()));

		// Open inventory
		for (Player player : players) {
			player.openInventory(inventory);
		}
	}

	public void place(int slot) {
		ItemStack item = turn == 0 ? Util.createItem(players[0].getName(), Material.SUGAR) :
				Util.createItem(players[1].getName(), Material.GLOWSTONE_DUST);
		inventory.setItem(slot, item);

		checkWin(turn);

		turn = turn == 0 ? 1 : 0;
	}
	
	public void sendMessage(String message) {
		for(Player all : players) {
			all.sendMessage(message);
		}
	}
	
	public void checkWin(int turn) {
		Material material = turn == 0 ? Material.SUGAR : Material.GLOWSTONE_DUST;

		// Win
		if (check(material, 1) && check(material, 2) && check(material, 3) ||
				check(material, 4) && check(material, 5) && check(material, 6) ||
				check(material, 7) && check(material, 8) && check(material, 9) ||
				check(material, 1) && check(material, 4) && check(material, 7) ||
				check(material, 2) && check(material, 5) && check(material, 8) ||
				check(material, 3) && check(material, 6) && check(material, 9) ||
				check(material, 1) && check(material, 5) && check(material, 9) ||
				check(material, 3) && check(material, 5) && check(material, 7)) {
			win(players[turn]);
			return;
		}
		// Draw
		for (int i = 1; i < 10; i++) {
			ItemStack item = inventory.getItem(i);
			if (item == null) {
				return;
			}
		}

		win(null);
	}

	public void win(Player player) {
		this.ended = true;

		if (player == null) {
			sendMessage(TicTacToe.getMessageFile().getMessage("result.draw"));

			// Add stats
			for (Player all : players) {
				Profile winnerProfile = Profile.getProfile(all.getUniqueId());
				winnerProfile.addGame();
				winnerProfile.addDraw();
			}
		} else {
			sendMessage(TicTacToe.getMessageFile().getMessage("result.win", player.getName()));

			// Add stats
			Profile winnerProfile = Profile.getProfile(player.getUniqueId());
			winnerProfile.addGame();
			winnerProfile.addWin();

			Profile loserProfile = Profile.getProfile(getOther(player).getUniqueId());
			loserProfile.addGame();
			loserProfile.addLose();
		}

		for (Player all : players) {
			if (all != null && all.isOnline()) {
				all.closeInventory();
			}
		}

		sendStats();
		TicTacToe.getGameManager().deleteGame(this);
	}

	public boolean hasEnded() {
		return ended;
	}

	public List<Player> getPlayers() {
		List<Player> list = new ArrayList<>();
		list.add(players[0]);
		list.add(players[1]);
		return list;
	}
	
	public Player getTurn() {
		return players[turn];
	}

	private Player getOther(Player player) {
		for (Player all : players) {
			if (!all.getUniqueId().equals(player.getUniqueId())) {
				return all;
			}
		}

		return null;
	}

	private boolean check(Material material, int slot) {
		return inventory.getItem(slot) != null && inventory.getItem(slot).getType() == material;
	}

	private void sendStats() {
		for (Player player : players) {
			Profile profile = Profile.getProfile(player.getUniqueId());

			String stats = TicTacToe.getMessageFile().getMessage("stats.hover", profile);

			TextComponent textComponent = new TextComponent(TicTacToe.getMessageFile().getMessage("stats.message"));
			textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(stats).create()));
			player.spigot().sendMessage(textComponent);

			player.playSound(player.getLocation(), Sound.LEVEL_UP, 10F, 10F);
		}
	}
}
