package de.paul2708.tictactoe.game;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class GameManager {

	private Queue queue;
	private List<Game> games;

	public GameManager() {
		this.queue = new Queue();
		this.games = new ArrayList<>();
	}

	public Game getGame(Player p) {
		for (Game game : games) {
			for (Player player : game.getPlayers()) {
				if (player.getUniqueId().equals(p.getUniqueId())) {
					return game;
				}
			}
		}

		return null;
	}
	
	public int getGames() {
		return games.size();
	}
	
	public List<Player> getPlayers() {
		List<Player> list = new ArrayList<>();
		for (Game game : games) {
			list.addAll(game.getPlayers());
		}
		
		return list;
	}
	
	public void createGame(Player first, Player second) {
		Game game = new Game(first, second);
		games.add(game);
	}

	public void deleteGame(Game game) {
		games.remove(game);
	}

	public Queue getQueue() {
		return queue;
	}
}
