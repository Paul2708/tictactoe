# TicTacToe Plugin for Minecraft
--------------------------------------------------
The plugin provides the possibility to play the well-known game
TicTacToe in Minecraft.
## Features
* simple setup, no commands are needed
* configure all messages
* statistic system
* queue system

## Setup
* Put the [`TicTacToe.jar`](https://www.spigotmc.org/resources/custom-tictactoe-1-8-1-11-many-settings-available.44929/download?version=175702) in your plugin folder
* Start the server and wait until all files were created
* Stop the server
* Setup the mysql login credentials (config.yml) and customize the messages (messages.yml)
* Start you're server again and connect

## How to play
Right-click a crafting table to join the queue. If a second player joins the queue, the game will start.
Leave the game by closing the inventory.

## Configuration
##### config.yml  
----------------
```
mysql:
    ip: localhost
    database: tictactoe
    user: root
    password: ''
```
##### messages.yml
----------------
```
tag: '&8[&eTicTacToe&8]'
game:
  against: '%tag% &7You are playing against &e%player%&7.'
  first_turn: '%tag% &e%player% &7starts.'
  not_your_turn: '%tag% &cIt''s not your turn.'
  click_nowhere: '%tag% &cPlease just use the workbench.'
queue:
  already_in: '%tag% &cYour are already in the queue.'
  added: '%tag% &aYou joined the queue. &7Waiting for player..'
result:
  draw: '%tag% &eDraw! - Nobody won.'
  win: '%tag% &e%player% has won the game.'
stats:
  hover: |-
    &7Your stats:
    &7Games: &e%games%
    &7Wins: &e%wins%
    &7Loses: &e%loses%
    &7Draws: &e%draws%
  message: '%tag% &eYour new statistics!'
```
## Pictures
![Crafting table](https://image.prntscr.com/image/DMzYxi0pSki6q5LrCgSuzg.png)
![Stats](https://image.prntscr.com/image/IAK3emYARFC34PfhM-g4kg.png)
-----------------------------
## Problems during installation?
Just write a PM to [@Paul2708](https://www.spigotmc.org/members/paul2708.44827/) on Spigot or contact me via Twitter ([@theplayerpaul](https://twitter.com/theplayerpaul)), Discord or Skype.
Thank you for your attention.